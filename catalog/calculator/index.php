<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Подбор пилы");
?>
<?
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*");
$arFilter = Array("IBLOCK_ID" => 29, "ACTIVE" => "Y");

$res = CIBlockElement::GetList(Array('ID' => 'ASC'), $arFilter, false, false, $arSelect);
while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arProps = $ob->GetProperties();
    $arFields['PROPERTY'] = $arProps;
    $arResult[] = $arFields;
}

CModule::IncludeModule("currency");
$arFilter = array(
    "CURRENCY" => "EUR"
);
$by = "date";
$order = "desc";

$db_rate = CCurrencyRates::GetList($by, $order, $arFilter);
while ($ar_rate = $db_rate->Fetch()) {
    $curseEur = $ar_rate["RATE"];
}

?>

<div class="box_calculator" curse-rates="<?= $curseEur ?>">
    <div class="calc-block">
        <div class="box_width part_calculator">
            <div class="title">Ширина полотна</div>
            <select class="width_val" name="width_val">
                <option value="" disabled="disabled" selected="selected">Выберити Ширину полотна</option>
                <? foreach ($arResult as $arItem): ?>
                    <option value="<?= $arItem['NAME'] ?>" id-val="<?= $arItem['ID'] ?>"
                            cof-price="<?= $arItem['PROPERTY']['VAL']['VALUE'] ?>"><?= $arItem['NAME'] ?></option>
                <? endforeach; ?>
            </select>
        </div>
        <div class="box_thickness part_calculator">
            <div class="title">Толщина, мм</div>
            <div class="box_select_thickness">
                <? foreach ($arResult as $arItem): ?>
                    <span class="id-val-<?= $arItem['ID'] ?>"><?= $arItem['PROPERTY']['thickness']['VALUE'] ?></span>
                <? endforeach; ?>
            </div>
        </div>
    </div>

    <div class="calc-block">
        <div class="box_tooth_pitch part_calculator">
            <div class="title">Возможный шаг зубьев</div>
            <? foreach ($arResult as $arItem): ?>
                <div class="id-val-<?= $arItem['ID'] ?> tooth_pitch">
                    <? foreach ($arItem['PROPERTY']['tooth_pitch']['VALUE'] as $arItemProp): ?>
                        <label>
                            <input type="radio" name="tooth_pitch_<?= $arItem['ID'] ?>"
                                   value="<?= $arItemProp ?>"><?= $arItemProp ?>
                        </label>
                    <? endforeach; ?>
                </div>
            <? endforeach; ?>
        </div>
    </div>
    <div class="calc-block">
        <div class="box_length part_calculator">
            <div class="title">Длина, мм.</div>
            <input type="text" name="length" class="length" placeholder="Длина в мм.">
        </div>
        <div class="box_price part_calculator">
            <div class="title">Цена за пилу, руб.</div>
            <div class="price"></div>
        </div>
    </div>

</div>
<div class="message_error"></div>
<div class="text-right ">
    <div class="btn btn-calc-order animated fadeIn" data-toggle="modal" data-target="#modalOrderSaw">Заказать пилу</div>
</div>

<div class="modal_block fadeIn animated none">
    <div class="windows_modal aplication_saw_order">
        <div class="window_close"></div>
        <div class="windows_title text-center">
            Оформить заказ
        </div>
        <div class="wrapper_form">
            <div class="value_block">
                <table width="100%" class="table">
                    <tr>
                        <th>Ширина полотна</th>
                        <th>Толщина</th>
                        <th>Шаг зубьев</th>
                        <th>Длина</th>
                    </tr>

                    <tr>
                        <td>
                            <div class="widthTd"></div>
                        </td>
                        <td>
                            <div class="thicknessTd"></div>
                        </td>
                        <td>
                            <div class="pitchTd"></div>
                        </td>
                        <td>
                            <div class="lenghtTd"></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="text-right">
                            <strong>Цена</strong>
                            <div class="price"><span class="glyphicon glyphicon-rub"></span></div>
                        </td>
                    </tr>
                </table>

            </div>
            <div class="form_block">
                <form action="">

                    <div class="form-group">
                        <label for="emailCalc">E-mail *</label>
                        <input type="text" name="email" id="emailCalc" class="form-control">
                        <div class="message_error"></div>
                    </div>
                    <div class="form-group">
                        <label for="phoneCalc">Телефон *</label>
                        <input type="text" name="phone" id="phoneCalc" class="form-control">
                        <div class="message_error"></div>
                    </div>
                    <div class="form-group">
                        <label for="fio">ФИО *</label>
                        <input type="text" name="fio" id="fio" class="form-control">
                        <div class="message_error"></div>
                    </div>
                    <div class="form-group">
                        <label for="commentCalc">Комментарий</label>
                        <textarea name="comment" id="commentCalc" class="form-control"></textarea>
                    </div>
                    <div class="message_error"></div>
                    <div class="text-right">
                        <div class="btn modal_button_submit">Отправить</div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<?
//$url = "http://www.cbr.ru/scripts/XML_daily.asp"; // URL, XML документ, всегда содержит актуальные данные
//$curs = array(); // массив с данными
//
//// функция полчуния даты из спарсенного XML
//function get_timestamp($date)
//{
//    list($d, $m, $y) = explode('.', $date);
//    return mktime(0, 0, 0, $m, $d, $y);
//}
//
//
//if(!$xml=simplexml_load_file($url)) die('Ошибка загрузки XML'); // загружаем полученный документ в дерево XML
//$curs['date']=get_timestamp($xml->attributes()->Date); // получаем текущую дату
//
//foreach($xml->Valute as $m){ // перебор всех значений
//    // для примера будем получать значения курсов лишь для двух валют USD и EUR
//    if($m->CharCode=="USD" || $m->CharCode=="EUR"){
//        $curs[(string)$m->CharCode]=(float)str_replace(",", ".", (string)$m->Value); // запись значений в массив
//    }
//}
//
//print_r($curs);
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
