<?
//Подключаем API битрикса
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');


//Отключаем статистику Bitrix
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

if ($_REQUEST['action']) {
    switch ($_REQUEST['action']) {
        case 'allDelBasket':
            if ((int)$_REQUEST['ID_BASKET']) {
                if (CModule::IncludeModule('catalog') && CModule::IncludeModule('sale')) {
                    CSaleBasket::DeleteAll($_REQUEST['ID_BASKET']);
                    $json = array(
                        'error' => false
                    );
                }
            } else {
                $json = array(
                    'error' => true,
                    'error_msg' => 'Basked ID is null',
                    'error_code' => 004
                );
            };
            break;
        case 'btnQuantityUpdate':
            if ((int)$_REQUEST['prodId']) {
                if (CModule::IncludeModule('catalog') && CModule::IncludeModule('sale')) {
                    $arFields = array(
                        "QUANTITY" => $_REQUEST['QUANTITY'],
                    );
                    CSaleBasket::Update($_REQUEST['prodId'], $arFields);
                    $json = array(
                        'error' => false
                    );
                }
            } else {
                $json = array(
                    'error' => true,
                    'error_msg' => 'Product ID is null',
                    'error_code' => 004
                );
            }
            break;
        case 'faqForm':

            CModule::IncludeModule('iblock');
            $el = new CIBlockElement;

            $PROP = array();
            $PROP['EMAIL'] = $_REQUEST['email'];
            $PROP['PHONE'] = $_REQUEST['phone'];

            $arLoadProductArray = Array(
                "IBLOCK_ID" => 26,
                "PROPERTY_VALUES" => $PROP,
                "NAME" => $_REQUEST['comment'],
                "ACTIVE" => "N",
            );

            if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
                $error = "New ID: " . $PRODUCT_ID;
                $json = array(
                    'error' => false,
                    'error_msg' => $error,
                );
            } else {
                $error = "Error: " . $el->LAST_ERROR;
                $json = array(
                    'error' => true,
                    'error_msg' => $error,
                );
            }


            break;

        case 'feedForm':
            CModule::IncludeModule('main');// подключение главного модуля

            $arEventFields = array(
                "EMAIL" => $_REQUEST['email'],
                "PHONE" => $_REQUEST['phone'],
                "COMMENT" => $_REQUEST['comment'],
            ); // собираем данные для отправки
            CEvent::Send("FeedBack", 's1', $arEventFields); // вызов функции отправки

            $json = array(
                'error' => false
            );

            break;

        case 'sawOrder':

			CModule::IncludeModule('iblock');

            $iblock_id = 30;
            $arFilter = Array("IBLOCK_ID"=>$iblock_id, "ACTIVE"=>"Y");
            $res_count = CIBlockElement::GetList(Array(), $arFilter, Array(), false, Array());

			$el = new CIBlockElement;

			$PROP = array();

			$PROP['email'] = $_REQUEST['email'];
			$PROP['phone'] = $_REQUEST['phone'];
			$PROP['width'] = $_REQUEST['width'];
			$PROP['thickness'] = $_REQUEST['thickness'];
			$PROP['pitch'] = $_REQUEST['pitch'];
			$PROP['lenght'] = $_REQUEST['lenght'];
			$PROP['price'] = $_REQUEST['price'];
			$PROP['comment'] = $_REQUEST['comment'];
			$PROP['contact'] = $_REQUEST['contact'];

			$arLoadProductArray = Array(
                "IBLOCK_ID" => 30,
                "PROPERTY_VALUES" => $PROP,
                "NAME" => $res_count+1,
				"ACTIVE" => "Y",
			);

			if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
                $error = "New ID: " . $PRODUCT_ID;


                CModule::IncludeModule('main');
                CModule::IncludeModule('sale');

                $arEventFields = array(
                    'ORDER_ID' => $PRODUCT_ID,
                    'ORDER_DATE' => date('d.m.Y'),
                    'SALE_EMAIL' => COption::GetOptionString("sale", "order_email")
                );

                $arEventFields['EMAIL'] = $_REQUEST['email'];
                $arEventFields['PHONE'] = $_REQUEST['phone'];
                $arEventFields['WIDTH'] = $_REQUEST['width'];
                $arEventFields['THICKNESS'] = $_REQUEST['thickness'];
                $arEventFields['PITCH'] = $_REQUEST['pitch'];
                $arEventFields['LENGHT'] = $_REQUEST['lenght'];
                $arEventFields['PRICE'] = $_REQUEST['price'];
                $arEventFields['COMMENT'] = $_REQUEST['comment'];
                $arEventFields['ORDER_USER'] = $_REQUEST['contact'];

                CEvent::Send("SAW_ORDER", 's1', $arEventFields);

                $json = array(
                    'error' => false,
                    'error_msg' => $error,
                );

            } else {
                $error = "Error: " . $el->LAST_ERROR;
                $json = array(
                    'error' => true,
                    'error_msg' => $error,
                );
            }

		break;

        default:
            $json = array(
                'error' => true,
                'error_msg' => 'Do not have an action',
                'error_code' => 002
            );
            break;
    };

} else {
    $json = array(
        'error' => true,
        'error_msg' => 'Do not have an action',
        'error_code' => 001,
    );
};

echo json_encode($json);

?>