<?




/*
You can place here your functions and event handlers

AddEventHandler("module", "EventName", "FunctionName");
function FunctionName(params)
{
	//code
}
*/
function GetRateFromCBR($CURRENCY)
{
    global $DB;
    global $APPLICATION;

    CModule::IncludeModule('currency');
    if(!CCurrency::GetByID($CURRENCY))
        //����� ������ ��� �� �����, ����� � ���� ������ ���������
        return false;

    $DATE_RATE=date("d.m.Y");//�������
    $QUERY_STR = "date_req=".$DB->FormatDate($DATE_RATE, CLang::GetDateFormat("SHORT", $lang), "D.M.Y");

    //������ ������ � www.cbr.ru � �������� ������ ���� �� �������� ����
    $strQueryText = QueryGetData("www.cbr.ru", 80, "/scripts/XML_daily.asp", $QUERY_STR, $errno, $errstr);

    //�������� XML � ������������ � ��������� �����
    $charset = "windows-1251";
    if (preg_match("/<"."\?XML[^>]{1,}encoding=[\"']([^>\"']{1,})[\"'][^>]{0,}\?".">/i", $strQueryText, $matches))
    {
        $charset = Trim($matches[1]);
    }
    $strQueryText = eregi_replace("<!DOCTYPE[^>]{1,}>", "", $strQueryText);
    $strQueryText = eregi_replace("<"."\?XML[^>]{1,}\?".">", "", $strQueryText);
    $strQueryText = $APPLICATION->ConvertCharset($strQueryText, $charset, SITE_CHARSET);

    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/xml.php");

    //������ XML
    $objXML = new CDataXML();
    $res = $objXML->LoadString($strQueryText);
    if($res !== false)
        $arData = $objXML->GetArray();
    else
        $arData = false;

    $NEW_RATE=Array();

    //�������� ���� ������ ������ $CURRENCY
    if (is_array($arData) && count($arData["ValCurs"]["#"]["Valute"])>0)
    {
        for ($j1 = 0; $j1<count($arData["ValCurs"]["#"]["Valute"]); $j1++)
        {
            if ($arData["ValCurs"]["#"]["Valute"][$j1]["#"]["CharCode"][0]["#"]==$CURRENCY)
            {
                $NEW_RATE['CURRENCY']=$CURRENCY;
                $NEW_RATE['RATE_CNT'] = IntVal($arData["ValCurs"]["#"]["Valute"][$j1]["#"]["Nominal"][0]["#"]);
                $NEW_RATE['RATE'] = DoubleVal(str_replace(",", ".", $arData["ValCurs"]["#"]["Valute"][$j1]["#"]["Value"][0]["#"]));
                $NEW_RATE['DATE_RATE']=$DATE_RATE;
                break;
            }
        }
    }

    if ((isset($NEW_RATE['RATE']))&&(isset($NEW_RATE['RATE_CNT'])))
    {

        //���� ��������, ��������, ���� �� �������� ���� ��� ���� �� �����, ���������
        CModule::IncludeModule('currency');
        $arFilter = array(
            "CURRENCY" => $NEW_RATE['CURRENCY'],
            "DATE_RATE"=>$NEW_RATE['DATE_RATE']
        );
        $by = "date";
        $order = "desc";

        $db_rate = CCurrencyRates::GetList($by, $order, $arFilter);
        if(!$ar_rate = $db_rate->Fetch())
            //������ ����� ���, ������ ���� �� �������� ����
            CCurrencyRates::Add($NEW_RATE);

    }

    //���������� ��� ������ �������, ����� ����� �� "������"
    file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/log.log", "$CURRENCY=".var_export($CURRENCY, true)."\r\n-----\r\n", FILE_APPEND);
    return 'GetRateFromCBR("'.$CURRENCY.'");';


}

?>