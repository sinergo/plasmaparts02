$(document).ready(function(){

   /* hideAutForm = function () {
        $('.login-form ').hide();
        $('.login-form').removeClass('open-login-form');
    };

    $('body').on('click', function (event) {
        hideAutForm();
    });
*/

    $('.login-desc').click(function(){

        if($('.login-form').hasClass('open-login-form')){
            $('.login-form').removeClass('open-login-form');
            $('.login-form ').hide();
            $('.overlay').css('display', 'none');
        } else {
            $('.login-form').addClass('open-login-form');
            $('.login-form ').show();
            $('.overlay').css('display', 'block');
        }
    });

    function windowSizeLogin(){
        if ($(window).width() <= '1200'){

            $('.login-tablet a').attr("href", "/auth/index.php");
        } else {
        }
    }

    $(window).on('load resize',windowSizeLogin);

});