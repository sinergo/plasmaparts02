<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strTitle = "";
?>
<div class="product-catalog">
    <div class="product-catalog-title text-uppercase">Каталог продукции <span class="catalog-arrow"></span></div>
    <div class="catalog-section-list none">
        <?
        $TOP_DEPTH = $arResult["SECTION"]["DEPTH_LEVEL"];
        $CURRENT_DEPTH = $TOP_DEPTH;

        foreach ($arResult["SECTIONS"] as $arSection)
        {
            $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
            $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
            if ($CURRENT_DEPTH < $arSection["DEPTH_LEVEL"]) {
                echo "\n", str_repeat("\t", $arSection["DEPTH_LEVEL"] - $TOP_DEPTH), "<ul>";
            } elseif ($CURRENT_DEPTH == $arSection["DEPTH_LEVEL"]) {
                echo "</li>";
            } else {
                while ($CURRENT_DEPTH > $arSection["DEPTH_LEVEL"]) {
                    echo "</li>";
                    echo "\n", str_repeat("\t", $CURRENT_DEPTH - $TOP_DEPTH), "</ul>", "\n", str_repeat("\t", $CURRENT_DEPTH - $TOP_DEPTH - 1);
                    $CURRENT_DEPTH--;
                }
                echo "\n", str_repeat("\t", $CURRENT_DEPTH - $TOP_DEPTH), "</li>";
            }

            $count = $arParams["COUNT_ELEMENTS"] && $arSection["ELEMENT_CNT"] ? "&nbsp;(" . $arSection["ELEMENT_CNT"] . ")" : "";

            if ($_REQUEST['SECTION_ID'] == $arSection['ID']) {
                $link = '<a href="' . $arSection["SECTION_PAGE_URL"] . '">' .'<b>' . $arSection["NAME"] . $count . '</b>'. '</a>';
                $strTitle = $arSection["NAME"];
            } else {
                $link = '<a href="' . $arSection["SECTION_PAGE_URL"] . '">' . $arSection["NAME"] . $count . '</a>';
            }

            echo "\n", str_repeat("\t", $arSection["DEPTH_LEVEL"] - $TOP_DEPTH);
                $arFilter = Array(
                    "IBLOCK_ID" => $arResult['IBLOCK_ID'],
                    "SECTION_ID" => $arSection['ID'],
                    "ACTIVE" => 'Y'
                );

                $countSection = CIBlockSection::GetCount($arFilter);
            ?>
            <li id="<?= $this->GetEditAreaId($arSection['ID']); ?>" <?if ($countSection):?>class="not_link"<?endif;?>><?= $link ?>
                <?
                if ($arSection["DEPTH_LEVEL"] <= 2) {
                    if ($countSection):
                        ?>
                        <span class="glyphicon glyphicon-menu-right"></span>
                    <?endif;?>
                    <?
                }



            $CURRENT_DEPTH = $arSection["DEPTH_LEVEL"];
        }

        while ($CURRENT_DEPTH > $TOP_DEPTH) {
            echo "</li>";
            ?>
            <li><a href="/catalog/calculator/">Подбор пилы</a></li>
            <?
            echo "\n", str_repeat("\t", $CURRENT_DEPTH - $TOP_DEPTH), "</ul>", "\n", str_repeat("\t", $CURRENT_DEPTH - $TOP_DEPTH - 1);
            $CURRENT_DEPTH--;
        }
        ?>

</div>
</div>
