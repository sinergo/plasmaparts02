$(document).ready(function () {

    $('.product-catalog-title').click(function () {
        if ($('.catalog-section-list').hasClass('none')) {
            $('.catalog-section-list').removeClass('none');
            $('.product-catalog-title').addClass('title_style');
            $('.overlay').css('display', 'block');
            $('.catalog').css('z-index', '10');
        } else {
            $('.catalog-section-list').addClass('none');
            $('.product-catalog-title').addClass('title_style');
            $('.product-catalog-title').removeClass('title_style');
            $('.overlay').css('display', 'none');
            $('.catalog').css('z-index', '1');
        }
    });


});
