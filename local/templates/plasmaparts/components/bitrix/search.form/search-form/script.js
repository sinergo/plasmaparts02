$(document).ready(function(){



    $('.search').on('click', function () {
        $('.overlay').css('display', 'block');
        $('.form-slide').stop().show().animate({'left': '0', 'opacity': '1'}, 700);
        $('.main-menu').css('z-index', '10');

    });

    $('.search-form-close').on('click', function () {
        $('.form-slide').stop().animate({'left': '600px', 'opacity': '0'}, 700, function () {
            $(this).hide();
        });
        $('.overlay').css('display', 'none');
        $('.main-menu').css('z-index', '0');
    });
});