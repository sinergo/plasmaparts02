<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?>
<div class="search-form">

        <div class="form-slide">
            <div class="flex-block">
                <form action="<?= $arResult["FORM_ACTION"] ?>">
                    <? if ($arParams["USE_SUGGEST"] === "Y"): ?><? $APPLICATION->IncludeComponent(
                        "bitrix:search.suggest.input",
                        "",
                        array(
                            "NAME" => "q",
                            "VALUE" => "",
                            "INPUT_SIZE" => 15,
                            "DROPDOWN_SIZE" => 10,
                        ),
                        $component, array("HIDE_ICONS" => "Y")
                    ); ?><? else: ?>
                        <input type="text" name="q" value="" size="15" placeholder="ПОИСК"/><? endif; ?>
                    <button name="s" type="submit" value=""><span class="glyphicon glyphicon-menu-right"></span>
                    </button>
                </form>
                <div class="search-form-close"></div>
            </div>
    </div>
</div>