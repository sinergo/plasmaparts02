<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$this->IncludeLangFile('template.php');
$cartId = $arParams['cartId'];

require(realpath(dirname(__FILE__)) . '/top_template.php');

if ($arParams["SHOW_PRODUCTS"] == "Y" && $arResult['NUM_PRODUCTS'] > 0) {
    ?>
    <div data-role="basket-item-list" class="bx-basket-item-list">

        <? if ($arParams["POSITION_FIXED"] == "Y"): ?>
            <div id="<?= $cartId ?>status" class="bx-basket-item-list-action"
                 onclick="<?= $cartId ?>.toggleOpenCloseCart()"><?= GetMessage("TSB1_COLLAPSE") ?></div>
        <? endif ?>


        <div id="<?= $cartId ?>products" class="bx-basket-item-list-container">

            <? foreach ($arResult["CATEGORIES"] as $category => $items):
            if (empty($items))
                continue;
            ?>
            <div class="bx-basket-item-list-item-status">
                <h3><?= GetMessage("TSB1_$category") ?></h3>
                <button name="BasketClear" class="basket-clear-button" onclick="<?=$cartId?>.allDelBasket(<?=CSaleBasket::GetBasketUserID()?>)">
                <span class="close-ico"></span>
                Очистить</button>
            </div>

            <div class="table-block">
                <table class="basket-table scroll-table">
                    <thead>
                    <tr class="fixed">
                        <th></th>
                        <th>Наименование</th>
                        <th>Цена</th>
                        <th width="20%">Количество</th>
                        <th>Сумма</th>
                        <th></th>
                    </tr>

                    </thead>

                    <tbody>


                    <? foreach ($items as $v): ?>
                    <!--<div class="bx-basket-item-list-item">-->
                    <tr>
                        <td>
                            <div class="bx-basket-item-list-item-img">
                                <? if ($arParams["SHOW_IMAGE"] == "Y" && $v["PICTURE_SRC"]): ?>
                                    <? if ($v["DETAIL_PAGE_URL"]): ?>
                                        <a href="<?= $v["DETAIL_PAGE_URL"] ?>"><img src="<?= $v["PICTURE_SRC"] ?>"
                                                                                    alt="<?= $v["NAME"] ?>"></a>
                                    <? else: ?>
                                        <img src="<?= $v["PICTURE_SRC"] ?>" alt="<?= $v["NAME"] ?>"/>
                                    <? endif ?>
                                <? endif ?>

                            </div>
                        </td>
                        <td>
                            <div class="item-name">
                                <? if ($v["DETAIL_PAGE_URL"]): ?>
                                    <a href="<?= $v["DETAIL_PAGE_URL"] ?>"><?= $v["NAME"] ?></a>
                                <? else: ?>
                                    <?= $v["NAME"] ?>
                                <? endif ?>
                            </div>
                        </td>
                        <td>
                            <div class="price">
                                <?= $v["BASE_PRICE"] ?><span class="glyphicon glyphicon-ruble"></span>
                            </div>
                        </td>

                        <? if (true):/*$category != "SUBSCRIBE") TODO */
                            ?>
                            <!--                        <div class="bx-basket-item-list-item-price-block">-->
                            <? /*if ($arParams["SHOW_PRICE"] == "Y"):?>
									<div class="bx-basket-item-list-item-price"><strong><?=$v["PRICE_FMT"]?></strong></div>
									<?if ($v["FULL_PRICE"] != $v["PRICE_FMT"]):?>
										<div class="bx-basket-item-list-item-price-old"><?=$v["FULL_PRICE"]?></div>
									<?endif?>
								<?endif*/
                            ?>
                            <? if ($arParams["SHOW_SUMMARY"] == "Y"): ?>
                            <!--<div class="bx-basket-item-list-item-price-summ">-->
                            <td>
                                <div class="quantity-item-block">
                                    <a href="javascript:void(0)"
                                       onclick="if (BX('<?= $v['ID'] ?>').value > 1){BX('<?= $v['ID'] ?>').value--; btnQuantityUpdate('<?= $v['ID'] ?>', BX('<?= $v['ID'] ?>').value);}"
                                       class="delete-button-item btn_quantity_update">-</a>
                                    <input type="text" name="<?= $v['ID'] ?>" value="<? echo $v["QUANTITY"] ?>"
                                           id="<?= $v['ID'] ?>" class="quantity-input">
                                    <a href="javascript:void(0)" onclick="BX('<?= $v['ID'] ?>').value++; btnQuantityUpdate('<?= $v['ID'] ?>', BX('<?= $v['ID'] ?>').value);"
                                       class="add-button-item btn_quantity_update">+</a>
                                </div>


                                <!--<input type="submit" name="<? /* echo $arParams["ACTION_VARIABLE"]."ADD2BASKET" */ ?>" value="шт. В корзину" class="add_b_item">-->

                                <? /*= $v["QUANTITY"] */ ?><!--</td>-->
                            <td>
                                <div class="price"><?= $v["BASE_PRICE"]*$v["QUANTITY"] ?> <span class="glyphicon glyphicon-ruble"></span>
                                </div>
                            </td>
                            <!--</div>-->
                        <? endif ?>
                            <!--</div>-->
                        <? endif ?>
                        <td align="right">
                            <div class="bx-basket-item-list-item-remove"
                                 onclick="<?= $cartId ?>.removeItemFromCart(<?= $v['ID'] ?>)"
                                 title="<?= GetMessage("TSB1_DELETE") ?>"></div>
                        </td>
                        <!--	</div>-->
                        <? endforeach ?>
                        <? endforeach ?>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                        <td>
                            <div class="sum-items">Итого:</div>
                        </td>
                        <td>
                            <div class="price"><?=str_replace('руб.', '', $arResult['TOTAL_PRICE'])?> <span class="glyphicon glyphicon-ruble"></span></div>
                        </td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <? if ($arParams["PATH_TO_ORDER"] && $arResult["CATEGORIES"]["READY"]): ?>
            <div class="bx-basket-item-list-button-container">
                <div>
                    <a href="#" class="btn btn-continue"><?= GetMessage("TSB1_CONTINUE") ?></a>
                    <p class="description-button">Вернуться к выбору товаров</p>
                </div>
                <div>
                    <a href="<?= $arParams["PATH_TO_BASKET"] ?>"
                       class="btn btn-go-to-basket"><?= GetMessage("TSB1_2ORDER") ?></a>
                    <p class="description-button">Оформить заказ</p>
                </div>
            </div>

        <? endif ?>
    </div>

    <script>
        BX.ready(function () {
            <?=$cartId?>.
            fixCart();
        });
    </script>
    <?
}