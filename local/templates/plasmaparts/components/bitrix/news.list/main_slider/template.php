<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<ul class="main-slider">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<li style="background-image: url('<?=$arItem['PREVIEW_PICTURE']['SRC']?>');">
			<div class="container">
				<div class="item">
					<div class="text-block">
						<p class="title"><?=$arItem['NAME']?></p>
						<p><?=$arItem['PREVIEW_TEXT']?></p>
					</div>
					<a class="button-slider" href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>">Подробнее</a>
				</div>
			</div>
		</li>
	<?endforeach;?>
</ul>
<div id="main-slider-pager">
	<div class="container">
		<div class="slider-pager-links">
			<?$i = 0;
			foreach($arResult["ITEMS"] as $arItem):
				?>
				<a data-slide-index="<?=$i?>" href="">
					<p class="text-uppercase"><?=$arItem['NAME']?></p>
					<p><?=$arItem['PROPERTIES']['SHORT_DESC']['VALUE']?></p>
				</a>
			<?$i++;
			endforeach;?>
		</div>
	</div>
</div>

