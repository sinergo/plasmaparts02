$(document).ready(function(){
    //news-slider
    $('.news-slider').bxSlider({
        mode: 'fade',
        auto: true,
        responsive: true,
        pager: false,
        controls: true,
        infiniteLoop: true
    });
});
