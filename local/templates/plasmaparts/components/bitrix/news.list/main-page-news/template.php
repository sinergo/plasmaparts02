<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!--<div class="news-list">-->
    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br/>
    <? endif; ?>
    <?$i=0; foreach ($arResult["ITEMS"] as $arItem): $i++;?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <?if ($i == 1):?>
            <div class="col-lg-6 col-md-6 col-sm-12 item">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="news-title"><?= $arItem["NAME"] ?></a>
                    <?
                    $date1 = $arItem['DATE_CREATE'];
                    $date = date('j', strtotime($date1)) . ' ' . strtolower(FormatDate("F", MakeTimeStamp($date1))) . ' ' . date('Y', strtotime($date1));
                    ?>
                    <div class="news-date"><?=$date?></div>
                    <div class="text-block"><p><?= $arItem["PREVIEW_TEXT"] ?></p></div>
            </div>
        <?else:?>
            <div class="col-lg-3 col-md-3 col-sm-6 item">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="news-title"><?= $arItem["NAME"] ?></a>
                    <?
                    $date1 = $arItem['DATE_CREATE'];
                    $date = date('j', strtotime($date1)) . ' ' . strtolower(FormatDate("F", MakeTimeStamp($date1))) . ' ' . date('Y', strtotime($date1));
                    ?>
                    <div class="news-date"><?=$date?></div>
                    <div class="text-block"><p><?= $arItem["PREVIEW_TEXT"] ?></p></div>
            </div>

        <?endif;?>

    <? endforeach; ?>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <br/><?= $arResult["NAV_STRING"] ?>
    <? endif; ?>
<!--</div>-->
