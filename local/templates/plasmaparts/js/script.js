$(document).ready(function () {
    //hide menu

    var siteMapToggle = $(".hide-menu-button");

    siteMapToggle.on("click", function () {
        $('.hide-menu').slideToggle();
        if (siteMapToggle.hasClass("-close")) {
            siteMapToggle.removeClass("-close");

        } else {
            siteMapToggle.addClass("-close");
        }

    });


    $('.main-slider').bxSlider({
        pagerCustom: '#main-slider-pager',
        controls: false,
        auto: true,
        responsive: true
    });

    $('.opening-block').on('click', function () {
        if ($(this).attr('data-open') == 'true') {
            $(this).attr('data-open', 'false').removeClass('block_style');
            $(this).next().stop().slideUp();
        } else {
            $(this).attr('data-open', 'true').addClass('block_style');
            $(this).next().stop().slideDown();
        }
    });
    if ($('.js-phone-input').length) {
        $('.js-phone-input').inputmask("+7 (999) 999-99-99");
    }

    $('#faq-btn').click(function () {
        $('.error-message').html('');
        $('.faq-form .form-control').removeClass('input-error');
        var error = false;

        if (!$('.faq-form #phoneNumber').val()) {
            error = true;
            $('.faq-form #phoneNumber').addClass('input-error');
            textError = $('.error-message').html();
            $('.error-message').html(textError + 'Введите номер телефона <br/>');
        }
        if (!$('.faq-form #eMail').val()) {
            error = true;
            $('.faq-form #eMail').addClass('input-error');
            textError = $('.error-message').html();
            $('.error-message').html(textError + 'Введите адрес электронной почты <br>');
        }
        if (!$('.faq-form #comment').val()) {
            error = true;
            $('.faq-form #comment').addClass('input-error');
            textError = $('.error-message').html();
            $('.error-message').html(textError + 'Введите вопрос');
        }

        if (!error) {
            var data = {
                action: 'faqForm',
                phone: $('.faq-form #phoneNumber').val(),
                email: $('.faq-form #eMail').val(),
                comment: $('.faq-form #comment').val()
            };
            $.ajax({
                url: '/api/',
                data: data,
                type: 'POST',
                success: function (json) {
                    if (!json.error) {
                        $('.faq-form').html('<div class="success-message">Ваш  вопрос принят</div>');
                    }

                }
            });
        }

    });


    $('#feedback-btn').click(function () {

        $('.error-message-feed').html('');
        var error = false;
        $('.feedback-form .form-control').removeClass('input-error');

        if (!$('.feedback-form #phoneNumberFeed').val()) {
            error = true;
            $('.feedback-form #phoneNumberFeed').addClass('input-error');
            $('.feedback-form #phoneNumberFeed').closest('.form-group').find('.error-message-feed').html('Введите номер телефона');
        }
        if (!$('.feedback-form #eMailFeed').val()) {
            error = true;
            $('.feedback-form #eMailFeed').addClass('input-error');
            $('.feedback-form #eMailFeed').closest('.form-group').find('.error-message-feed').html('Введите адрес электронной почты');
        }
        if (!$('.feedback-form #commentFeed').val()) {
            error = true;
            $('.feedback-form #commentFeed').addClass('input-error');
            $('.feedback-form #commentFeed').closest('.form-group').find('.error-message-feed').html('Введите вопрос');
        }


        if (!error) {
            var data = {
                action: 'feedForm',
                phone: $('.feedback-form #phoneNumberFeed').val(),
                email: $('.feedback-form #eMailFeed').val(),
                comment: $('.feedback-form #commentFeed').val()
            };
            $.ajax({
                url: '/api/',
                data: data,
                type: 'POST',
                success: function (json) {
                    if (!json.error) {
                        $('.feedback-form').html('<div class="success-message">Мы вам перезвоним</div>');
                    }

                }
            });
        }
    });

    $('.select-city').click(function () {

        if ($('.city-block-select').hasClass('-open')) {
            $('.city-block-select').removeClass('-open');
            $('.overlay').css('display', 'none');
            $('.product-catalog').css('z-index', '10');
        } else {
            $('.city-block-select').addClass('-open');
            $('.overlay').css('display', 'block');
            $('.city-block').css('z-index', '10');
            $('.product-catalog').css('z-index', '0')

        }
    });


    //канкулятор пил
    /*$('#modalOrderSaw').modal();*/

    $('.box_calculator .width_val').change(function () {
        $('.message_error').html('');
        $('.calc-block').removeClass('error-block-style');
        var idVal = $(this).find("option:selected").attr('id-val') * 1;
        var cofPrice = $(this).find("option:selected").attr('cof-price') * 1;
        $('.box_calculator .box_thickness .box_select_thickness span').removeClass('display_yes');
        $('.box_calculator .box_thickness .id-val-' + idVal).addClass('display_yes');

        $('.box_calculator .box_tooth_pitch .tooth_pitch').removeClass('display_yes');
        $('.box_calculator .box_tooth_pitch .tooth_pitch.id-val-' + idVal).addClass('display_yes');

        $('.box_calculator .box_tooth_pitch .tooth_pitch input[type=radio]:checked').prop('checked', false);

        if ($('.box_calculator .box_length .length').val()) {
            var lengthVal = $('.box_calculator .box_length .length').val() * 1;
            calculatorFormul(idVal, lengthVal, cofPrice);
        } else {

        }
    });

    $('.box_calculator .box_length .length').on('input', function () {
        $('.message_error').html('');
        $('.calc-block').removeClass('error-block-style');
        if ($('.box_calculator .width_val').find("option:selected").val()) {
            var idVal = $('.box_calculator .width_val').find("option:selected").attr('id-val') * 1;
            var cofPrice = $('.box_calculator .width_val').find("option:selected").attr('cof-price') * 1;
            if ($(this).val()) {
                var lengthVal = $(this).val() * 1;
                calculatorFormul(idVal, lengthVal, cofPrice);
            } else {
                $('.box_price .price').html('');
            }
        } else {

        }
    });

    $('.btn-calc-order').click(function () {

        if ($('.box_calculator .box_price .price').html() && $('.box_calculator .box_tooth_pitch .tooth_pitch.display_yes').find('input[type=radio]:checked').val()) {
            $('.aplication_saw_order .widthTd').html($('.box_calculator .width_val').find("option:selected").val());
            $('.aplication_saw_order .thicknessTd').html($('.box_calculator .box_select_thickness .display_yes').html());
            $('.aplication_saw_order .pitchTd').html($('.box_calculator .box_tooth_pitch .tooth_pitch.display_yes').find('input[type=radio]:checked').val());
            $('.aplication_saw_order .lenghtTd').html($('.box_calculator .box_length .length').val());
            $('.aplication_saw_order .price').html($('.box_calculator .box_price .price').html());

            if ($('.modal_block').hasClass('none')) {
                $('.modal_block').removeClass('none');
                $('.modal_block').addClass('animated fadeIn');
                $('.overlay_basket').css('display', 'block');
            } else {
                $('.modal_block').addClass('none');
                $('.overlay_basket').css('display', 'none');
            }

        } else {
            $('.box_calculator + .message_error').html('* Укажите необходимые параметры пилы');
            $('.calc-block').addClass('error-block-style');

        }
    });

    $('.window_close').click(function () {
        $('.modal_block').addClass('none');
        $('.overlay_basket').css('display', 'none');
    });

    $('.modal_button_submit').click(function () {

        $('.message_error').html('');
        var error = false;
        $('.form_block .form-control').removeClass('error-block-style');

        if (!$('.form_block #emailCalc').val()) {
            error = true;
            $('.form_block #emailCalc').addClass('error-block-style');
            $('.form_block #emailCalc').closest('.form-group').find('.message_error').html('Введите адрес электронной почты');
        }

        if (!$('.form_block #phoneCalc').val()) {
            error = true;
            $('.form_block #phoneCalc').addClass('error-block-style');
            $('.form_block #phoneCalc').closest('.form-group').find('.message_error').html('Введите номер телефона');
        }

        if (!$('.form_block #fio').val()) {
            error = true;
            $('.form_block #fio').addClass('error-block-style');
            $('.form_block #fio').closest('.form-group').find('.message_error').html('Введите фамилию имя отчество');
        }

        if (!error) {
            var data = {
                action: 'sawOrder',
                email: $('.form_block #emailCalc').val(),
                phone: $('.form_block #phoneCalc').val(),
                contact: $('.form_block #fio').val(),
                comment: $('.form_block #commentCalc').val(),
                width: $('.aplication_saw_order .widthTd').html(),
                thickness: $('.aplication_saw_order .thicknessTd').html(),
                pitch: $('.aplication_saw_order .pitchTd').html(),
                lenght: $('.aplication_saw_order .lenghtTd').html(),
                price: $('.aplication_saw_order .price').html()
            };
            $.ajax({
                url: '/api/',
                data: data,
                type: 'POST',
                success: function (json) {
                    if (!json.error) {
                        $('.modal_block .wrapper_form').html('<div class="success-message">Заявка отправлена! <div class="reload-message">Страница будет обновлена через <span></span></div> </div>');
                        var i = 5;
                        $('.success-message span').html(i);
                        setInterval(function () {
                            i--;
                            $('.success-message span').html(i);
                        }, 1000);

                        setTimeout(function () {
                            location.reload();
                        }, 5000);
                    }

                }
            });
        }

    });
    ///канкулятор пил

// overlay close

    var overlayClose = $('.overlay');
    overlayClose.click(function (event) {
        e = event || window.event
        if (e.target == this) {
            $('.overlay').css('display', 'none');
            $('.catalog-section-list').addClass('none');
            $('.product-catalog-title').removeClass('title_style');
            $('.city-block-select ').removeClass('-open');
            $('.form-slide').hide();
            $('.main-menu').css('z-index', '0');
            $('.login-form ').hide();
            $('.login-form').removeClass('open-login-form');
        }
    });


});

function calculatorFormul(idVal, lengthVal, cofPrice) {
    var curseEur = $('.box_calculator').attr('curse-rates');
    var price = ((cofPrice * 1.18) / 1000 * lengthVal * curseEur + 150) * 1.45;
    $('.box_price .price').html((price).toFixed(2));
}