<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if ($_GET["city"]) {
    setcookie('city', $_GET["city"], time() + 7 * 24 * 60 * 60);
    LocalRedirect();
}

?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><? $APPLICATION->ShowTitle() ?></title>
    <link href="<?= SITE_TEMPLATE_PATH ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= SITE_TEMPLATE_PATH ?>/css/animate.css" rel="stylesheet">

    <script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery-3.1.1.min.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/bootstrap.min.js"></script>

    <script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.bxslider.min.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/inputmask.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/inputmask.extensions.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.inputmask.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/script.js"></script>

    <link rel="shortcut icon" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/favicon.ico" type="image/x-icon"/>
    <link rel="apple-touch-icon" sizes="57x57" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/apple-icon-57x57.png"/>
    <link rel="apple-touch-icon" sizes="72x72" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/apple-icon-72x72.png"/>


    <meta name="theme-color" content="#B40000">

    <link rel="apple-touch-icon" sizes="57x57" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/apple-icon-180x180.png">

    <link rel="icon" type="image/png" sizes="36x36"
          href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/android-icon-36x36.png">
    <link rel="icon" type="image/png" sizes="48x48"
          href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/android-icon-48x48.png">
    <link rel="icon" type="image/png" sizes="72x72"
          href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/android-icon-72x72.png">
    <link rel="icon" type="image/png" sizes="96x96"
          href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/android-icon-96x96.png">
    <link rel="icon" type="image/png" sizes="144x144"
          href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/android-icon-144x144.png">
    <link rel="icon" type="image/png" sizes="192x192"
          href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/android-icon-192x192.png">

    <link rel="icon" type="image/png" sizes="32x32" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/manifest.json">

    <? $APPLICATION->ShowHead() ?>
    <link href="<?= SITE_TEMPLATE_PATH ?>/css/media.css" rel="stylesheet">
</head>

<body>


<? $APPLICATION->ShowPanel(); ?>
<div class="overlay"></div>
<div class="overlay_basket"></div>
<header>

    <div class="top-block">
        <div class="container">
            <div class="flex-block-top">
                <div class="left-block">
                    <div class="city-block">
                        <span class="select-city">
                            <?
                            if($_COOKIE["city"]) {
                                echo $_COOKIE["city"];
                            } else {
                                echo "Новокузнецк";
                            }
                            ?>
                            </span>
                        <div class="city-block-select">
                            <ul>
                                <?
                                CModule::IncludeModule("iblock");
                                $arSelect = Array("ID", "NAME");
                                $arFilter = Array("IBLOCK_ID" => 28, "ACTIVE" => "Y");
                                $res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
                                while ($ob = $res->GetNextElement()) {
                                    $arFields = $ob->GetFields();
                                    echo "<li><a href='?city=" . $arFields['NAME'] . "'><span>" . $arFields['NAME'] . "</span></a></li>";
                                }
                                ?>
                            </ul>
                        </div>


                    </div>
                </div>
                <div class="right-block">
                    <div class="phone-block">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            ".default",
                            array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => "/includes/header_phone.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            false
                        ); ?>
                    </div>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:system.auth.form",
                        "plasmaparts-authorization",
                        array(
                            "COMPONENT_TEMPLATE" => "plasmaparts-authorization",
                            "REGISTER_URL" => "/personal/profile/?register=yes",
                            "FORGOT_PASSWORD_URL" => "",
                            "PROFILE_URL" => "/personal/profile/",
                            "SHOW_ERRORS" => "N"
                        ),
                        false
                    ); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="main-menu">
        <div class="container">
            <div class="flex-block-top">
                <a href="/" class="logo" title="На главную"></a>
                <div class="inline-menu">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "plasmaparts-menu",
                        array(
                            "COMPONENT_TEMPLATE" => "plasmaparts-menu",
                            "ROOT_MENU_TYPE" => "top",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => array(),
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "left",
                            "USE_EXT" => "N",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N"
                        ),
                        false
                    ); ?>
                </div>
                <div class="search-hide-button-block">
                    <div class="hide-menu-button ">
                        <span></span>
                    </div>
                    <div class="search"></div>
                </div>
                <? $APPLICATION->IncludeComponent("bitrix:search.form", "search-form", Array(),
                    false
                ); ?>
            </div>
        </div>
    </div>
    <div class="hide-menu">
        <div class="container">
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "plasmaparts-menu",
                array(
                    "COMPONENT_TEMPLATE" => "plasmaparts-menu",
                    "ROOT_MENU_TYPE" => "top",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(),
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                false
            ); ?>
        </div>
    </div>
    <div class="main-slider-block">
        <div class="catalog <? if ($APPLICATION->GetCurPage(false) !== '/'): ?> static-position<? endif; ?>">
            <div class="container">
                <div class="flex-block">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section.list",
                        "tree",
                        array(
                            "COMPONENT_TEMPLATE" => "tree",
                            "IBLOCK_TYPE" => "CATALOG",
                            "IBLOCK_ID" => "27",
                            "SECTION_ID" => "",
                            "SECTION_CODE" => "",
                            "COUNT_ELEMENTS" => "N",
                            "TOP_DEPTH" => "3",
                            "SECTION_FIELDS" => array(
                                0 => "",
                                1 => "",
                            ),
                            "SECTION_USER_FIELDS" => array(
                                0 => "",
                                1 => "",
                            ),
                            "VIEW_MODE" => "LINE",
                            "SHOW_PARENT_NAME" => "Y",
                            "SECTION_URL" => "/catalog/#SECTION_CODE_PATH#/",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_GROUPS" => "Y",
                            "ADD_SECTIONS_CHAIN" => "Y"
                        ),
                        false
                    ); ?>

                    <div class="links-block">
                        <? $APPLICATION->IncludeComponent("bitrix:news.list", "fast_link", Array(
                            "ACTIVE_DATE_FORMAT" => "",    // Формат показа даты
                            "ADD_SECTIONS_CHAIN" => "N",    // Включать раздел в цепочку навигации
                            "AJAX_MODE" => "N",    // Включить режим AJAX
                            "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
                            "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                            "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                            "AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
                            "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
                            "CACHE_GROUPS" => "Y",    // Учитывать права доступа
                            "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                            "CACHE_TYPE" => "A",    // Тип кеширования
                            "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
                            "DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                            "DISPLAY_BOTTOM_PAGER" => "N",    // Выводить под списком
                            "DISPLAY_DATE" => "Y",    // Выводить дату элемента
                            "DISPLAY_NAME" => "Y",    // Выводить название элемента
                            "DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
                            "DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
                            "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
                            "FIELD_CODE" => array(    // Поля
                                0 => "",
                                1 => "",
                            ),
                            "FILTER_NAME" => "",    // Фильтр
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
                            "IBLOCK_ID" => "25",    // Код информационного блока
                            "IBLOCK_TYPE" => "CONTENT",    // Тип информационного блока (используется только для проверки)
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",    // Включать инфоблок в цепочку навигации
                            "INCLUDE_SUBSECTIONS" => "Y",    // Показывать элементы подразделов раздела
                            "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                            "NEWS_COUNT" => "3",    // Количество новостей на странице
                            "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                            "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
                            "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                            "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
                            "PAGER_TEMPLATE" => ".default",    // Шаблон постраничной навигации
                            "PAGER_TITLE" => "Новости",    // Название категорий
                            "PARENT_SECTION" => "",    // ID раздела
                            "PARENT_SECTION_CODE" => "",    // Код раздела
                            "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
                            "PROPERTY_CODE" => array(    // Свойства
                                0 => "",
                                1 => "SHORT_DESC",
                                2 => "",
                            ),
                            "SET_BROWSER_TITLE" => "N",    // Устанавливать заголовок окна браузера
                            "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
                            "SET_META_DESCRIPTION" => "N",    // Устанавливать описание страницы
                            "SET_META_KEYWORDS" => "N",    // Устанавливать ключевые слова страницы
                            "SET_STATUS_404" => "N",    // Устанавливать статус 404
                            "SET_TITLE" => "N",    // Устанавливать заголовок страницы
                            "SHOW_404" => "N",    // Показ специальной страницы
                            "SORT_BY1" => "ID",    // Поле для первой сортировки новостей
                            "SORT_BY2" => "ID",    // Поле для второй сортировки новостей
                            "SORT_ORDER1" => "ASC",    // Направление для первой сортировки новостей
                            "SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
                            "COMPONENT_TEMPLATE" => "main-page-news"
                        ),
                            false
                        ); ?>


                    </div>
                </div>
            </div>
        </div>
        <? if ($APPLICATION->GetCurPage(false) === '/'): ?>
            <div class="main_slider">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "main_slider",
                    array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FIELD_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "FILTER_NAME" => "",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => "24",
                        "IBLOCK_TYPE" => "CONTENT",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "MESSAGE_404" => "",
                        "NEWS_COUNT" => "3",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Новости",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "PROPERTY_CODE" => array(
                            0 => "SHORT_DESC",
                            1 => "LINK",
                            2 => "",
                        ),
                        "SET_BROWSER_TITLE" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "N",
                        "SHOW_404" => "N",
                        "SORT_BY1" => "SORT",
                        "SORT_BY2" => "ID",
                        "SORT_ORDER1" => "ASC",
                        "SORT_ORDER2" => "ASC",
                        "COMPONENT_TEMPLATE" => "main_slider"
                    ),
                    false
                ); ?>
            </div>
        <? endif; ?>

    </div>


</header>
<div class="main-content <? if ($APPLICATION->GetCurPage(false) === '/'): ?> padding-bottom-0<? endif; ?>">
    <div class="container">
        <? if ($APPLICATION->GetCurPage(false) !== '/'): ?>
            <div class="breadcrumbs-block">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:breadcrumb",
                    "breadcrumbs-template",
                    array(
                        "PATH" => "",
                        "SITE_ID" => "s1",
                        "START_FROM" => "0",
                        "COMPONENT_TEMPLATE" => "breadcrumbs-template"
                    ),
                    false
                ); ?>
            </div>
        <? endif; ?>

        <h1 id="pagetitle"
            <? if ($APPLICATION->GetCurPage(false) === '/'): ?>style="overflow: hidden; height: 0; padding: 0; margin: 0; border: none;"<? endif; ?>><? $APPLICATION->ShowTitle(false) ?></h1>


        <? if (strpos($APPLICATION->GetCurDir(), '/personal/') !== false): ?>
            <div class="personal-menu-block">
                <? $APPLICATION->IncludeComponent("bitrix:menu", "personal-menu", Array(
                    "ROOT_MENU_TYPE" => "left",    // Тип меню для первого уровня
                    "MENU_CACHE_TYPE" => "A",    // Тип кеширования
                    "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                    "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
                    "MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
                    "MAX_LEVEL" => "1",    // Уровень вложенности меню
                    "CHILD_MENU_TYPE" => "",    // Тип меню для остальных уровней
                    "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "DELAY" => "N",    // Откладывать выполнение шаблона меню
                    "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
                    "COMPONENT_TEMPLATE" => "menu_top",
                    "MENU_THEME" => "site"
                ),
                    false
                ); ?>
            </div>
        <? endif; ?>
